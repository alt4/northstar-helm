#!/bin/bash

# Slightly altered version of L1ghtman#6732's script
if [ ! -e /mnt/titanfall/Titanfall2.exe ]; then
    echo "Downloading Titanfall2 Files"

    curl -L "https://ghcr.io/v2/nsres/titanfall/manifests/2.0.11.0-dedicated-mp" -s -H "Accept: application/vnd.oci.image.manifest.v1+json" -H "Authorization: Bearer QQ==" | jq -r '.layers[]|[.digest, .annotations."org.opencontainers.image.title"] | @tsv' |
    {
    paths=()
    uri=()
    while read -r line; do
        while IFS=$'\t' read -r digest path; do
        path="/mnt/titanfall/$path"
        folder=${path%/*}
        mkdir -p "$folder"
        touch "$path"
        paths+=("$path")
        uri+=("https://ghcr.io/v2/nsres/titanfall/blobs/$digest")
        done <<< "$line" ;
    done
    parallel --link --jobs 5 'wget -O {1} {2} --header="Authorization: Bearer QQ==" -nv' ::: "${paths[@]}" ::: "${uri[@]}"
    }
else
    echo "Nothing to do"
fi

echo "Done"