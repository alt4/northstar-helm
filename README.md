# Northstar Helm Chart

[Northstar](https://github.com/R2Northstar/Northstar) is `a modding framework client that allows users to host their own Titanfall 2 servers using custom scripts and assets to create custom content, as well as being able to host vanilla content`.

## Introduction

This chart bootstraps a [Northstar](https://github.com/pg9182/northstar-dedicated) server on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

This chart has been tested to work on a k3s instance.

## Prerequisites

- Kubernetes 1.12+
- Helm 2.11+ or Helm 3.0-beta3+
- Titanfall's game files available in a PVC. Documentation pending, in the mean time [refer to the image's instructions](https://github.com/pg9182/northstar-dedicated#quick-start)

## Installing the Chart

To install the chart with the release name `vanilla`:

```console
kubectl create namespace titanfall
helm install -n titanfall vanilla .
```

These commands deploy the servers on the Kubernetes cluster in the default configuration. The [Parameters](#parameters) section lists the parameters that can be configured during installation.

Do note that, obviously, several releases with different settings (gamemodes, mods, etc...) can be published on the same cluster.

> **Tip**: List all releases using `helm list -n titanfall`

## Uninstalling the Chart

To uninstall/delete the `vanilla` deployment:

```console
helm delete -n titanfall vanilla
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

## Parameters

The following tables lists the configurable parameters of this chart and their default values.

| Parameter | Description | Default |
| --------- | ----------- | ------- |
| `TODO`    | TOOD        | `TODO`  |
|           |             |         |

A YAML file that specifies the values for the parameters can be provided while installing the chart. For example,

```console
helm install -n titanfall vanilla -f values.yaml .
```

> **Tip**: You can use the default [values.yaml](values.yaml) as a basis

## Configuration and installation details

### Prepare Titanfall's game files for use

TODO

## Roadmap

Bucket list of features that may eventually make it into the chart:

- Mod support on a per-server basis
- Tests?
- Network improvements (remove hostPorts and use services/ingresses)
